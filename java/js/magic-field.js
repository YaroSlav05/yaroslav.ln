//генерация случайного слова из масива 
var words = ["динозавр", "телефон", "столовая", "компьютер", "рыба", "телевизор"];
var word = words [Math.floor(Math.random () * words.length)];

//предстовляем слово в виде черточек
var answerArray = [];
for ( var i = 0; i < word.length; i++) {
    answerArray [i] = "_";
}

var remainingLetters = word.length; //осталось  не отгадыных букв

//игровой цикл начало
while (remainingLetters > 0) { 
    //alert ( 'Количество букв: ' + answerArray.join (" ") );
    var guess = prompt ("Угадайте букву: " + answerArray.join (" ") + " или нажмите отмена для выхода" );
    if (guess === null) {                //Если отмена
        break;                          //Прекратить цикл
    } else if ( guess.length !== 1) {  //Если не равно 1 
        alert ("Пожалуйста введите только одну букву");
    } else {  //Если введена одна буква то включается цикл 
        for ( var j = 0; j < word.length; j++) { //цикл идет по длине загадоного слова с каждой попыткой
            if (word[j] === guess) {            //сравнивает совпадения 
                answerArray[j] = guess;        //добавляет в ответ совпадения
                remainingLetters--;           //при совпадение отминусовывает остаток букв
            }
        }
    }
}
//игровой цикл конец
alert ("Отлично: было загадано слово: " + word);